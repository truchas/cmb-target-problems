CMB target problem 4: Heat tranfer with view factor radiation: mold preheat
---------------------------------------------------------------------------

&OUTPUTS
  output_t = 0.0 5000.0
  output_dt = 300.0
/

&PROBE
 data = 'temperature'
 data_file = 'tc3.dat'
 description = 'mold 60 deg'
 coord = 0.0427, 0.0, 0.074
/

&PROBE
 data = 'temperature'
 data_file = 'tc9.dat'
 description = 'core pole'
 coord = 0.0, 0.0, 0.0739
/

&PROBE
 data = 'temperature'
 data_file = 'tc11.dat'
 description = 'core 60 deg'
 coord = 0.0371, 0.0, 0.0642
/

&MESH
 mesh_file = 'mesh4.gen'
 coordinate_scale_factor = 0.001
 interface_side_sets = 24, 30, 40, 41, 42, 50, 51
/

&PHYSICS
 heat_transport = T
 electromagnetics = T
 materials = 'graphite', 'graphite-felt', 'VOID'
/

&DIFFUSION_SOLVER
 abs_temp_tol = 0.0
 rel_temp_tol = 1.0e-3
 abs_enthalpy_tol = 0.0
 rel_enthalpy_tol = 1.0e-3
 max_nlk_itr = 5
 nlk_tol = 0.02
 nlk_preconditioner = 'hypre_amg'
 pc_amg_cycles = 2
 verbose_stepping = .true.
/

&NUMERICS
 dt_init = 0.01
 dt_grow = 5.0
 dt_max = 60.0
/

### INDUCTION HEATING ##########################################################

&ALTMESH
 altmesh_file = 'em-mesh.gen'
 altmesh_coordinate_scale_factor = 0.001
 !grid_transfer_file = 'mesh-mapping.bin'
/

&ELECTROMAGNETICS
 em_domain_type = 'quarter_cylinder'
 source_frequency = 2000
 steps_per_cycle = 20
 maximum_source_cycles = 3
 ss_stopping_tolerance = 0.01
 maximum_cg_iterations = 500
 cg_stopping_tolerance = 1.0e-8
 output_level = 2
 num_etasq = 1.0e-10
/

&INDUCTION_COIL
 center = 0.0, 0.0, 0.205 ! [m] = 0.285 - 0.0254 * (6.25/2))
 radius = 0.150 ! [m] = 0.0254 * 11.75
 length = 0.133 ! [m] = 0.0254 * (6.25 - 1)
 nturns = 4
 current = 2000
/

### ENCLOSURE RADIATION ########################################################

Radiation within the space enclosed by the inner mold and base plate.

&ENCLOSURE_RADIATION
 name = 'inner'
 enclosure_file = 'inner-encl.re'
 ambient_constant = 300.0  ! unused (but required)
/

&ENCLOSURE_SURFACE
 enclosure_name = 'inner'
 name = 'inner-graphite'
 face_block_ids = 10, 11
 emissivity_constant = 0.8
/

### ENCLOSURE RADIATION ########################################################

Radiation within the connected space enclosed by the insulation. This includes
the void-filled casting volume. This is a partial enclosure with a gap at the
bottom between the base plate and the insulation. The gap is top surface of
another graphite plate (unmodeled) and the ambient temperature for view factor
radiation should be the temperature of that plate.

&ENCLOSURE_RADIATION
 name = 'outer'
 enclosure_file = 'outer-encl.re'
 ambient_constant = 300.0
/

&ENCLOSURE_SURFACE
 enclosure_name = 'outer'
 name = 'graphite'
 face_block_ids = 20, 21, 22, 23, 24
 emissivity_constant = 0.8
/

&ENCLOSURE_SURFACE
  enclosure_name = 'outer'
  name = 'outer-insulation'
  face_block_ids = 25
  emissivity_constant = 0.75
/

### BODIES #####################################################################

# Casting -- initially void
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 1
  material_name = 'VOID'
  temperature = 300.0
/

# Graphite mold, funnel, and base
&BODY
 surface_name = 'from mesh file'
 mesh_material_number = 2, 3, 4, 5
 material_name = 'graphite'
 temperature = 300
/

# Felt insulation
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 6
  material_name = 'graphite-felt'
  temperature = 300
/

### BOUNDARY CONDITIONS #######################################################

# No flux on all x=0 and y=0 surfaces (symmetry)

&THERMAL_BC
 name = 'symmetry'
 face_set_ids = 1
 type = 'flux'
 flux = 0.0
/

&THERMAL_BC
 name = 'base-bottom'
 face_set_ids = 2
 type = 'htc'
 htc = 50.0
 ambient_temp = 300.0
/

&THERMAL_BC
 name = 'insulation-bottom'
 face_set_ids = 3
 type = 'flux'
 flux = 0
/

&THERMAL_BC
 name = 'insulation-to-furnace'
 face_set_ids = 4
 type = 'radiation'
 emissivity = 0.75
 ambient_temp = 300.0
/

Top of the casting void. Insignificant, but must specify something.
&THERMAL_BC
  name = 'casting-top'
  face_set_ids = 6
  type = 'flux'
  flux = 0.0
/

Rest of the external boundary is coupled through enclosure radiation.

### INTERFACE CONDITIONS #######################################################

&THERMAL_BC
 name = 'base-core'
 face_set_ids = 30
 type = 'interface-htc'
 htc = 500.0
/

&THERMAL_BC
 name = 'core-case-gap'
 face_set_ids = 40
 type = 'gap-radiation'
 emissivity = 0.75
/

&THERMAL_BC
 name = 'core-case'
 face_set_ids = 41, 42
 type = 'interface-htc'
 htc = 500.0
/

&THERMAL_BC
 name = 'funnel-case'
 face_set_ids = 50, 51
 type = 'interface-htc'
 htc = 200.0
/

&THERMAL_BC
 name = 'mold-casting'
 face_set_ids = 24
 type = 'interface-htc'
 htc = 0.0
/

### MATERIALS ##################################################################

### GRAPHITE ###

&MATERIAL
 name = 'graphite'
 density = 1750
 specific_heat_func = 'graphite-Cp'
 conductivity_func = 'graphite-k'
 electrical_conductivity = 8.4e4
/

&FUNCTION
 name = 'graphite-Cp'
 type = 'polynomial'
 poly_coefficients   = 2.406e2,  2.50167, -1.1231e-3,  1.7e-7
 poly_exponents(1,:) = 0, 1, 2, 3
/

&FUNCTION
 name = 'graphite-k'
 type = 'polynomial'
 poly_coefficients   = 112.3504, -0.13876097,  9.042e-5, -2.0e-8
 poly_exponents(1,:) = 0, 1, 2, 3
/

### GRAPHITE FELT ###

&MATERIAL
  name = 'graphite-felt'
  density = 80
  specific_heat = 1670
  conductivity = 0.1 !0.2 !0.43
/
