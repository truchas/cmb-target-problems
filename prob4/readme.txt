PROB4: INDUCTION HEATING WITH VIEW FACTOR RADIATION
---------------------------------------------------

Uses heat transfer, induction heating, and enclosure radiation physics models.
Also adds some temperature output probes.

* The mesh is `mesh4.gen`. Needs to be scaled by 0.001 (mm -> m).

### View factor calculation (genre)

* There are two independent enclosures:

  * Inner enclosure inside the mold core
    - enclosure formed by side sets 10 and 11
    - ignore the casting volume (block 1) which is empty (void)
    - apply 'MirrorX' and 'MirrorY' symmetries to get the full model
    - non-blocking and complete (no holes)
    - use a hemicube resolution of 500 and minimum separation of 10

  * Outer enclosure is the remaining connected void space inside the insulation
    - enclosure formed by side sets 20, 21, 22, 23, 24, 25
    - ignore the casting volume (block 1) which is empty (void)
    - apply 'MirrorX' and 'MirrorY' symmetries to get the full model
    - this is a blocking and partial enclosure (has holes)
    - partial area is 2.78e-3
    - use a hemicube resolution of 500 and minimum separation of 10

### Truchas computational model

* 3 materials plus void.

  * Graphite (single phase) assigned to element blocks 2, 3, 4, 5.
    - Density 1750; electrical conductivit 8.4e4
    - specific heat: 2.406e2 + 2.5016 T - 1.1231e-3 T^2 + 1.e-7 T^3
    - thermal conductivity: 112.3504 - 0.138776 T + 9.042e-5 T^2 - 2e-8 T^3

  * Graphite felt (single phase) assigned to element blocks 6 and 7
    - Density 80, specific heat 1670, thermal conductivity 0.1

  * Void assigned to element block 1 (would be filled with fluid next sim stage)

* Initial temperature 300 for all element blocks

* Thermal boundary conditions (simplified)

  - No heat flux on side set 1 (symmetry planes)
  - HTC on side set 2: coef 50; ref temp 300
  - Simple radiation on side set 4 : emissivity 0.75, amb temp 300
  - No heat flux on side set 6 (top of casting void - req something)

* Thermal interface conditions (simplified)

  - 0 HTC on side set 24 (between mold and void casting body)
  - HTC on side sets 30, 41, 42: coef 500
  - HTC on side sets 50, 51: coef 200
  - gap radiation on side sets 40: emissivity 0.75

* EM problem description:

  - mesh file "em-mesh.gen"
  - EM domain type is "quarter_cylinder"
  - 4-turn coil with radius 0.150 and length 0.133.
    Centered on the z-axis at z=0.205
  - Current is 2000 A, and the frequency is 2000 Hz

* Two enclosure radiation problems:

  * Inner (between the mold core and base)
    - surfaces 10, 11; emissivity 0.8
    - radiation enclosure file "inner-encl.re"

  * Outer (everything else)
    - ambient temperature 300 (seen through hole in enclosure)
    - surfaces 20, 21, 22, 23, 24; emissivity 0.8
    - surface 25; emissivity 0.75
    - radiation enclosure file "outer-encl.re"

* Numerical parameters

  - relative temperature and enthalpy tolerance: 1e-3
  - absolute temperature and enthalpy tolerance: 0
  - initial time step: 1e-2
  - dt_grow = 5.0
  - dt_max = 60

* Start at t=0, output every 300 sec to t = 5000

* Simplified temperature probe output:

  - core pole, "tc9.dat" at (0,0,0.0739)
  - core 60 degrees, "tc11.dat", (0.0371,0,0.642)
  - case 60 degrees, "tc3, dat", (0.0427,0,0.74)
