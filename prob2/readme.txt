PROB2A
--------------------------------------------------------------------------------

* Heat conduction with electromagnetic induction heating.

* The main mesh is `mesh1.gen`. Needs to be scaled by 0.001 (mm -> m).

* The EM mesh is `em-mesh.gen`. Needs to be scaled by 0.001 (mm -> m).

* There is a single (1-phase) graphite material assigned to all element
  blocks (2, 3, 4, 5). Density 1750; electrical conductivity 8.4e4; and
  temperature dependent specific heat Cp and conductivity k:
  * Cp(T) = 2.406e2 + 2.50167 T - 1.1231e-3 T^2 + 1.7e-7 T^3
  * k(T) = 112.3504 - 0.13876097 T + 9.042e-5 T^2 - 2.0e-8 T^3

* Initial temperature 300 all bodies.

* Thermal boundary conditions

  * No heat flux on sides set 1 (symmetry planes)
  * HTC on side set 2: coef 50; ref temp 300
  * Simple radiation on side sets 20, 21, 22: emissivity 0.75, amb temp 300
  * No heat flux on side sets 10, 11, 23

* Thermal interface conditions

  - HTC on side set 30: coef 500
  - Gap radiation on sideset 40: emissivity 0.75
  - HTC on side sets 41, 42: coef 500
  - HTC on side sets 50, 51: coef 200

* Heat conduction numerical parameters

  - relative temperature and enthalpy tolerance: 1e-3
  - absolute temperature and enthalpy tolerance: 0
  - initial time step: 1e-2
  - dt_grow = 5.0

* EM problem description:

  - EM domain type is "quarter_cylinder"
  - 2 different 4-turn coils with radius 0.150 and length 0.133.
    Both centered on the z-axis: one at z=0.211 and the other at z=0.199.
  - Current in each coil is 1000, and the frequency is 2000 Hz

* EM numerical parameters

  - CG stopping tolerance: 1e-8
  - num_etasq: 1e-10
  - max cg iterations: 500
  - output level: 2

* Start at t=0, output every 60 sec to t = 600, then every 300 sec to
  final time 3600.
