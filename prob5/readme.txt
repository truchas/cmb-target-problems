PROB5A: BASIC ISOTHERMAL FLOW -- MOLD FILLING
---------------------------------------------

Uses only the flow physics model.

* The mesh is `mesh3.gen`. Needs to be scaled by 0.001 (mm -> m). This
  consists of just the casting volume (element block 1).

* Just a single 1-phase material (liquid uranium) plus void.
  Uranium density 17.5e3 and viscosity 5e-3.

* The casting volume is initially empty (void).

* Flow boundary conditions:

  - Free-slip on symmetry planes (side set 1)
  - no-slip on walls of the mold (side set 24)
  - velocity boundary condition on side set 5. This is the inlet for the
    liquid uranium stream. The inlet velocity is (0,0,-0.35) between the
    initial time t=0 and t=16.2. It then ramps down to 0 at t=16.3.

* Flow model / numerics

  - gravitational body force: (0,0,-9.8)
  - viscous flow
  - use a courant limit of 0.4 (controls the time step size)
  - use material priority: uranium first, then void

* Solve to t=18 and output every 0.2 sec. Use an initial time step of 1e-3
  and set dt_grow = 1.05.


PROB5B: COUPLED FLOW/HEAT TRANSFER: MOLD FILLING
------------------------------------------------

This adds heat transfer in the fluid and mold to PROB5A, and is essentially
a marriage of PROB3A and PROB5A.

* The mesh is `mesh2.gen`. Needs to be scaled by 0.001 (mm -> m).

* Material properties from PROB3A

* PROB3A defines the heat transfer problem
  - use an HTC of 2000 for the mold/casting interface (side set 24)

* PROB5A defines the flow model and configuration of the casting volume (1)
  - the inflow material temperature is 1573
  - inflow material is the liquid uranium phase

* Heat transfer numerical parameters (different than PROB3A)
  - use the solver that is compatible with free-surface flow
  - relative residual tolerance of 1e-5, and NKL tolerance of 0.1
  - maximum number of NLK iterations is 50
  - use the Hypre preconditioner with 2 cycles

* Like PROB5A, solve to t=18 (or earlier for a shorter run time) and output
  every 0.2 sec. Use an initial time step of 1e-3 and set dt_grow to 1.05.
